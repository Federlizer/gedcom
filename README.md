# Gedcom

This repository contains my humble attempt to create a [GEDCOM 5.5.5](https://www.gedcom.org/index.html) reader/writer.



What do I need to know right now?

- How can I make a state machine that is able to parse this type of file?
- What kind of constraints do I have on the different types of concepts? E.g.:
    - What are the constraints on the lines themselves?
        - Leading spaces are illegal
        - Empty lines are illegal
        - There must be at least one line
        - Line length constraints? 255 including level number, cross-reference number, tag, value, delimiters and terminator
        - Line terminator constraints
            - Shall be one of (CR), (LF) or (CR/LF)
            - Shall not be (LF/CR)
            - Shall be consistent throughout the whole file
            - Every line, including the last one shall include a terminator
    - What are the constraints on the levels?
        - Shall be between 0 and 99
        - Shall not contain a leading 0
        - New level shall be no higher than previous level + 1
    - What are the constraints on the tags?
        - Tag length shall be a maximum of 31 code units (symbols)
        - 




## Requirements for the Gedcom standard


### Tags

tag:=
    [ [ U+005F ] + alphanum | tag + alphanum ]
where:
    U+005F = _ (underscore)


- Tags must be case sensitive
- Tag length must be maximum of 31 code units (aka symbols), including the optional underscore


### Terminators

terminator:=
    [ carriage_return | line_feed | carriage_return + line_feed ]
where:
    carriage_return = U+000D, the Carriage Return character (\r)
    line_feed = U+000A, the Line Feed character (\n)

- Terminators must be one of the following:
    - Carriage Return (CR, \r) == U+000D
    - Line Feed       (LF, \n) == U+000A
    - CR/LF              \r\n
    - NOTE: LF/CR terminator must not be used

- All terminators must be the same throughout the whole file
- Every line of a gedcom file must include a terminator


### Level numbers

- Levels must be between 0 and 99
- Levels must not contain leading 0
- Each new level must be no higher than the previous level + 1


