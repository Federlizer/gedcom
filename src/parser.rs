use regex::Regex;

use crate::{gedcom_file::GedcomFile, record::Record};

const CARRIAGE_RETURN: char = '\u{000D}';
const LINE_FEED: char = '\u{000A}';

const NULL: char = '\u{0000}';
const EOF: char = NULL;

#[derive(PartialEq)]
pub enum TerminatorType {
    CR,
    LF,
    CRLF,
    Unknown,
}

#[derive(Debug, PartialEq)]
pub enum ParsingError {
    MixedTerminators,
    LFCRTerminator,
    MissingLastTerminator,
}

pub struct Parser {
    input: Vec<char>,

    curr_position: usize,
    peek_position: usize,

    curr_char: char,

    terminator_type: TerminatorType,
}

impl Parser {
    pub fn new(input: &str) -> Self {
        let input_chars: Vec<char> = input.chars().collect();

        let mut s = Self {
            input: input_chars,

            curr_position: 0,
            peek_position: 0,

            curr_char: EOF,

            terminator_type: TerminatorType::Unknown,
        };

        s.advance_char();
        return s;
    }

    /// Advances the curr and peek positions and chars.
    ///
    /// If the end of the input has been reached, curr_char and peek_char will always
    /// be set to `'\0'` (EOF).
    fn advance_char(&mut self) {
        if self.peek_position >= self.input.len() {
            self.curr_char = EOF;
        } else {
            self.curr_char = self.input[self.peek_position]
        }

        self.curr_position = self.peek_position;
        self.peek_position = self.peek_position + 1;
    }

    /// Peeks into the next character of the input. It may return `'\0'` (EOF).
    fn peek_char(&self) -> char {
        if self.peek_position >= self.input.len() {
            return EOF;
        } else {
            return self.input[self.peek_position];
        }
    }

    /// Parses and verifies the terminator at the end of a gedcom line. Parsing the terminator
    /// means that `curr_char` will be advanced to the first non-terminator character of the input.
    ///
    /// Requires the `curr_char` to already be on the first terminator character (`\r` | `\n`).
    ///
    /// If the terminator type of this gedcom file hasn't been detected yet (i.e. `terminator_type`
    /// is set to `TerminatorType::Unknown`), this function will set the parsed terminator type to
    /// the parser.
    ///
    /// If the terminator type is not `TerminatorType::Unknown` the parsed terminator will be
    /// checked against the already detected `terminator_type` and return an error if there's a
    /// missmatch.
    fn parse_and_verify_terminator(&mut self) -> Result<(), ParsingError> {
        let parsed_terminator_type = match self.curr_char {
            LINE_FEED => {
                if self.peek_char() == CARRIAGE_RETURN {
                    return Err(ParsingError::LFCRTerminator);
                }
                TerminatorType::LF
            }

            CARRIAGE_RETURN => {
                if self.peek_char() == LINE_FEED {
                    self.advance_char();
                    TerminatorType::CRLF
                } else {
                    TerminatorType::CR
                }
            }

            // We should never get here
            ch => panic!("Unknown terminator char: '{}'", ch),
        };

        match self.terminator_type {
            TerminatorType::Unknown => {
                self.terminator_type = parsed_terminator_type;
            }

            _ => {
                if self.terminator_type != parsed_terminator_type {
                    return Err(ParsingError::MixedTerminators);
                }
            }
        };

        self.advance_char();
        Ok(())
    }

    fn read_line(&mut self) -> Result<String, ParsingError> {
        let mut line = String::new();

        while self.curr_char != LINE_FEED && self.curr_char != CARRIAGE_RETURN {
            // We must always be able to find a terminator before the EOF
            if self.curr_char == EOF {
                return Err(ParsingError::MissingLastTerminator);
            }

            line.push(self.curr_char);
            self.advance_char();
        }

        self.parse_and_verify_terminator()?;
        return Ok(line);
    }

    pub fn parse_gedcom_file(&mut self) -> Result<GedcomFile, ParsingError> {
        let mut file = GedcomFile::new();

        let level_re = r"^(?<level>([0-9])|([1-9][0-9]+)) ";
        let tag_re = r"(?<tag>[a-zA-Z]+)";
        let value_re = r"( (?<value>[a-zA-Z0-9\.]+))?$";

        let re = format!("{}{}{}", level_re, tag_re, value_re);

        let re = Regex::new(&re).expect("Should have been able to parse regex");

        // Start processing the file character by character, ending at terminator
        while self.curr_char != EOF {
            let line = self.read_line()?;

            let Some(captures) = re.captures(&line) else {
                panic!("Couldn't regex line {}", line);
            };

            let Some(level) = captures.name("level") else {
                panic!("Couldn't match level in line {}", line);
            };

            let Some(tag) = captures.name("tag") else {
                panic!("Couldn't match tag in line {}", line);
            };

            let value = captures.name("value").map(|v| v.as_str().to_string());
            let level = level
                .as_str()
                .parse::<usize>()
                .expect("Should have been able to parse to u32");
            let tag = tag.as_str().to_string();

            file.records.push(Record { level, tag, value });
        }

        Ok(file)
    }
}

#[cfg(test)]
mod test {
    use super::Record;
    use super::{Parser, ParsingError};

    #[test]
    fn it_advances_chars() {
        let input = String::from("Hello world!");
        let input_chars: Vec<char> = input.chars().collect();

        let mut p = Parser::new(&input);

        for i in 0..input.len() {
            assert_eq!(p.curr_position, i, "Current position not accurate");
            assert_eq!(p.peek_position, i + 1, "Peek position not accurate");

            assert_eq!(p.curr_char, input_chars[i], "Current char not accurate");

            if i >= input.len() - 1 {
                assert_eq!(p.peek_char(), '\0', "Peek char not accurate");
            } else {
                assert_eq!(p.peek_char(), input_chars[i + 1], "Peek char not accurate");
            }

            p.advance_char();
        }
    }

    #[test]
    fn errors_on_lf_cr_terminators() {
        let mut input = String::new();
        input.push_str("0 HEAD\n\r");
        input.push_str("1 GEDC\n\r");
        input.push_str("2 VERS 5.5.5\n\r");

        let mut p = Parser::new(&input);
        let result = p.parse_gedcom_file();

        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), ParsingError::LFCRTerminator)
    }

    #[test]
    fn parses_cr_terminators() {
        let mut input = String::new();
        input.push_str("0 HEAD\r");
        input.push_str("1 GEDC\r");
        input.push_str("2 VERS 5.5.5\r");

        let mut p = Parser::new(&input);
        let result = p.parse_gedcom_file();

        assert!(result.is_ok());
        let ged_file = result.unwrap();

        assert_eq!(ged_file.records.len(), 3);
        assert_eq!(
            ged_file.records[0],
            Record {
                level: 0,
                tag: "HEAD".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[1],
            Record {
                level: 1,
                tag: "GEDC".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[2],
            Record {
                level: 2,
                tag: "VERS".to_string(),
                value: Some("5.5.5".to_string()),
            }
        );
    }

    #[test]
    fn parses_lf_terminators() {
        let mut input = String::new();
        input.push_str("0 HEAD\n");
        input.push_str("1 GEDC\n");
        input.push_str("2 VERS 5.5.5\n");

        let mut p = Parser::new(&input);
        let result = p.parse_gedcom_file();

        assert!(result.is_ok());
        let ged_file = result.unwrap();

        assert_eq!(ged_file.records.len(), 3);
        assert_eq!(
            ged_file.records[0],
            Record {
                level: 0,
                tag: "HEAD".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[1],
            Record {
                level: 1,
                tag: "GEDC".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[2],
            Record {
                level: 2,
                tag: "VERS".to_string(),
                value: Some("5.5.5".to_string()),
            }
        );
    }

    #[test]
    fn parses_cr_lf_terminators() {
        let mut input = String::new();
        input.push_str("0 HEAD\r\n");
        input.push_str("1 GEDC\r\n");
        input.push_str("2 VERS 5.5.5\r\n");

        let mut p = Parser::new(&input);
        let result = p.parse_gedcom_file();

        assert!(result.is_ok());
        let ged_file = result.unwrap();

        assert_eq!(ged_file.records.len(), 3);
        assert_eq!(
            ged_file.records[0],
            Record {
                level: 0,
                tag: "HEAD".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[1],
            Record {
                level: 1,
                tag: "GEDC".to_string(),
                value: None,
            }
        );
        assert_eq!(
            ged_file.records[2],
            Record {
                level: 2,
                tag: "VERS".to_string(),
                value: Some("5.5.5".to_string()),
            }
        );
    }

    #[test]
    fn errors_on_missing_last_terminator() {
        let mut input = String::new();
        input.push_str("0 HEAD\n");
        input.push_str("1 GEDC\n");
        input.push_str("2 VERS 5.5.5");

        let mut p = Parser::new(&input);
        let result = p.parse_gedcom_file();

        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), ParsingError::MissingLastTerminator);
    }
}
