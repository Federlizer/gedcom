use crate::record;

#[derive(Debug, PartialEq)]
pub struct GedcomFile {
    pub records: Vec<record::Record>,
}

impl GedcomFile {
    pub fn new() -> Self {
        Self {
            records: Vec::new(),
        }
    }
}
