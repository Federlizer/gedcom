mod gedcom_file;
mod parser;
mod record;

fn main() {
    println!("Hello world!");
    let input = include_str!("../sample_files/gedcom_sample.ged");
    println!("{input}");

    let mut parser = parser::Parser::new(input);
    let _result = parser.parse_gedcom_file();
    // println!("{:?}", result);
}

/*
gedcom_line:=
    level + [ delim + xref_ID ] + delim + tag + [ delim + line_value ] + terminator


escape:=
    U+0040 + U+0023 + escape_text + U+0040

    where:
    U+0040 = @
    U+0023 = #

    A GEDCOM escape sequence begins and ends with an at sign.
    An escape sequence must not contain any at sign and must not be followed by an at sign; an
    escape sequence must be followed by either a delim (the space character) or a terminator.

level:=
    [ digit | non_zero_digit + digit ]

    The level number consists of one or two digits. It may be zero, but must not start with
    leading zeroes.
 */
