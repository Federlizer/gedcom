#[derive(Debug, PartialEq)]
pub struct Record {
    pub level: usize,
    pub tag: String,
    pub value: Option<String>,
}
